# Wall of Fame application for the Uni(versi)ty Tour 2015 #

### What is this repository for? ###

* This is the Wall of Fame application for the Uni(versi)ty Tour 2015
* Version 0.1
* Visit Wall of Fame Website on http://walloffame.azurewebsites.net/ to manage games and rounds

### How do I get set up? ###

* Visit [Downloads area](https://bitbucket.org/rherlt/walloffame/downloads) to get the latest Unity client library called *WallOfFame.Client.Net35*
* Now go to your Unity project folder and open *Assets* folder, in that folder make *new folder* called *Plugins*.
* Paste the content of the *WallOfFame.Client.Net35* zip archive in that folder and switch to yout Unity project.
* Wait until unity generates all meta informations
* Use the library as any C# library.



### Get started ###

* Make sure that your Unity projects runs under .NET 2.0 full profile (not *.NET 2.0 Subset*): File => BuildSettings and choose *PC, Mac, Linux Standalone* => PlayerSettings => Set *Api Compatibility Level* to .NET 2.0.
* Visit [Games](http://walloffame.azurewebsites.net/Game) area on Wall of Fame website, greate a new game and copy your *Game Id*
* Generate new C# script or insert the api calls to yout existing scipts.
* Add usings of the client library at top of your script:

```
#!c#
// [...]
using WallOfFame.Client.Net35;
using WallOfFame.Client.Shared.OperationContracts;
// [...]
```

* Create a property of the WofClient class:

```
#!c#

    public static WofClient WofClient 
    {
	  get;
	  private set;
    }
```

* Initialize the Property in the *Start()* method of your scipt:


```
#!c#
    void Start() 
    {        
        // Do other stuff
        // [..]

        // Initialize wall of fame API client with your game id, generated in step 1.
        WofClient = new WofClient ("00000000-0000-0000-0000-000000000000");
    }

```

* At the beginning of your game or at the beginning of a new round, call *StartRound()*


```
#!c#
    // [...]
    // Call at a new round of your game.
    WofClient.StartRound();
    // [...]
```

* If the game is finished, you can push the score and some player informations to the wall of fame website by calling the method *EndRound();*. The Arguments *playerName*, *playerComment* and *score* are **required**. In the return value of type *PublishRoundResponse* you can get some informations if the post was successful, such as HTTP Status code or response message of the server.


```
#!c#

    // [...]
    // Call after the round of your game has finished. Arguments:
    // WofClient.EndRound(string playerName, string playerComment, long score); 
    PublishRoundResponse serverResponse = WofClient.EndRound("John Doe", "wohooooo that was an awesome game and my new record", 2323453455);
```
* Visit [Rounds](http://walloffame.azurewebsites.net/Round) area of the Wall of Fame website to see posted rounds. 
* Have fun!