﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WallOfFame.Client.Net35;
using System.Threading;

namespace WallOfFame.Client.UnitTest
{
    [TestClass]
    public class WofClientTest
    {
        [TestMethod]
        public void LifeCycleTest1()
        {

            string username = "Rico";
            string comment = "UnitTest Result";
            long score = 3;
            string id = "5ed30fba-a9b8-4664-9b4a-c597ec1b9c5f";
                     
            WofClient client = new WofClient(id);
            client.StartRound();
            Thread.Sleep(3000);
            var result = client.EndRound(username, comment, score);

            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [TestMethod]
        public void GetAll()
        {

           string id = "5ed30fba-a9b8-4664-9b4a-c597ec1b9c5f";

            WofClient client = new WofClient(id);            
            var result = client.GetRounds();

            Assert.IsTrue(result != null && result.Rounds != null && result.Rounds.Count > 0);
        }

        [TestMethod]
        public void GetAllByCreatedDate()
        {

            string id = "5ed30fba-a9b8-4664-9b4a-c597ec1b9c5f";

            WofClient client = new WofClient(id);
            var result = client.GetRounds(DateTime.Now.AddDays(-5));

            Assert.IsTrue(result != null && result.Rounds != null && result.Rounds.Count > 0);
        }

        [TestMethod]
        public void GetById()
        {

            string id = "5ed30fba-a9b8-4664-9b4a-c597ec1b9c5f";
            Guid roundId = new Guid("dba8b836-2efb-4615-aab0-156ac70228f9");
            WofClient client = new WofClient(id);
            var result = client.GetRound(roundId);

            Assert.IsTrue(result != null && result.Round != null && result.Round.Id.HasValue);
        }
    }
}
