﻿using System;
using WallOfFame.Client.Shared.DataContracts;
using WallOfFame.Client.Shared.Interfaces;
using WallOfFame.Client.Shared;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using WallOfFame.Client.Shared.OperationContracts;
using System.Collections.Generic;
using System.Net;
using System.IO;

namespace WallOfFame.Universal.Client
{
    public class WofClient : IWofClient
    {
        public WofClient(string gameId)
        {
            try
            {
                GameId = new Guid(gameId);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("The argument gameId is not a valid Guid.", ex);
            }

        }

        public Guid GameId { get; private set; }
        private PublishRound _round;

        public void StartRound()
        {
            _round = new PublishRound
            {
                Start = DateTime.Now,
                GameId = GameId,
            };
        }


        public PublishRoundResponse EndRound(string playerName, string playerComment, long score)
        {
            _round.End = DateTime.Now;
            _round.PlayerComment = playerComment;
            _round.Playername = playerName;
            _round.Score = score;

            try
            {
                var response = PostData(Const.PostRoundUrl, _round);
                var responseString = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                var result = new PublishRoundResponse
                {
                    HttpStatusCode = response.StatusCode,
                    IsSuccessStatusCode = (response.StatusCode == HttpStatusCode.OK),
                    Message = response.ReasonPhrase
                };

                return result;

            }
            catch (Exception ex)
            {

                var result = new PublishRoundResponse
                {
                    HttpStatusCode = null,
                    IsSuccessStatusCode = false,
                    Message = ex.Message
                };

                return result;
            }
        }

        private static HttpResponseMessage PostData(string url, object body)
        {
            return HttpRequest(url, body, HttpMethod.Post);
        }

        private static HttpResponseMessage GetData(string url)
        {
            return HttpRequest(url, null, HttpMethod.Get);
        }

        private static HttpResponseMessage HttpRequest(string url, object body, HttpMethod method)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response;
            if (method == HttpMethod.Post)
            {
                string postData = JsonConvert.SerializeObject(body, Formatting.Indented);

                response = client.PostAsync(string.Format("{0}{1}", Const.BaseUrl, url), new StringContent(postData, Encoding.UTF8, "application/json")).GetAwaiter().GetResult();
            }
            else //HttpMethod.Get
            {
                response = client.GetAsync(string.Format("{0}{1}", Const.BaseUrl, url)).GetAwaiter().GetResult();
            }
            return response;
        }

        public GetRoundsResponse GetRounds()
        {
            return GetRounds(null);
        }

        public GetRoundsResponse GetRounds(DateTime? minCreatedDate)
        {

            string url = (minCreatedDate.HasValue) ? string.Format(Const.GetRoundsByMinCreatedUrl, string.Format("{0:yyyy}{0:MM}{0:dd}{0:HH}{0:mm}{0:ss}", minCreatedDate.Value)) : Const.GetRoundsUrl;
            var response = GetData(url);
            var responseString = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            try
            {
                var rounds = JsonConvert.DeserializeObject<GetRoundsResponse>(responseString);
                return rounds;
            }
            catch (Exception ex)
            {

                return new GetRoundsResponse() { Rounds = new List<Round>() };
            }
        }

        public GetRoundResponse GetRound(Guid id)
        {
            string url = string.Format(Const.GetRoundUrl, id.ToString("D"));
            var response = GetData(url);
            var responseString = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            try
            {
                var round = JsonConvert.DeserializeObject<GetRoundResponse>(responseString);
                return round;
            }
            catch (Exception)
            {
                return new GetRoundResponse() { Round = new Round() };
            }
        }
    }
}
