﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WallOfFame.Client.Shared.DataContracts;
using WallOfFame.Universal.Client;
using WallOfFame.Universal.Utils;
using WallOfFame.Universal.ViewModels.Base;
using Windows.UI.Xaml;

namespace WallOfFame.Universal.ViewModels
{
    public class MainViewModel : BaseViewModel 
    {

        public MainViewModel()
        {
            RefreshAsync();
            Rounds = new List<Round>();
            Headline = "Wall of Fame Universal App running on Windows 10 IOT @ Raspberry Pi 2 ";
            _timer = new DispatcherTimer{
                Interval = new TimeSpan(0,1,0)                
            };
            _timer.Tick += (o, e) => {
                RefreshAsync();
            };
            _timer.Start();
        }      

        private DispatcherTimer _timer;
        public ICommand RefreshCommand
        {
            get
            {
                return new ActionCommand((o) => RefreshAsync());
            }
        }

        private string _headline;
        public string Headline
        {
            get { return _headline; }
            set
            {
                SetField(ref _headline, value, "Headline");
            }
        }

        private List<Round> _rounds;
        public List<Round> Rounds {
            get { return _rounds; }
            private set
            {
                //use SetField from BaseViewModel
            }
        }

        public void RefreshAsync()
        {
            new TaskFactory().StartNew(delegate
            {

                WofClient client = new WofClient(Guid.Empty.ToString("D"));
                var rounds = client.GetRounds().Rounds.OrderByDescending(x => x.Created).ToList();
                SetField(ref _rounds, rounds, "Rounds");
            }).ConfigureAwait(false);
        }
    }
}
