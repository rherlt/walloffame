﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WallOfFame.Client.Shared.DataContracts;
using Windows.UI.Xaml.Data;

namespace WallOfFame.Universal.Utils
{
    class TimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is PublishRound)
            {
                PublishRound round = value as PublishRound;

                if (round.End.HasValue && round.Start.HasValue)
                {
                    TimeSpan? time = round.End - round.Start;

                    if(time.HasValue)
                        return string.Format("{0:00}:{1:00}:{2:00}", time.Value.Hours, time.Value.Minutes, time.Value.Seconds);
                }
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
