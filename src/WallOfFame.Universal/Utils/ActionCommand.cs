﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WallOfFame.Universal.Utils
{
    public class ActionCommand : ICommand
    {
        public ActionCommand(Action<object> action, Func<bool> canExcecute)
        {
            _action = action;
            _canExcecute = canExcecute;
        }
        public ActionCommand(Action<object> action)
        {
            _action = action;
            _canExcecute = () => true;
        }


        private Action<object> _action;
        private Func<bool> _canExcecute;

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return _canExcecute();
        }

        public void Execute(object parameter)
        {
            _action(parameter);
        }
    }
}
