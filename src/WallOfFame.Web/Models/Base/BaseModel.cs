﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallOfFame.Web.Models.Base
{
    public abstract class BaseModel
    {
        public Guid Id { get; set; }

        protected BaseModel()
        {
            Id = Guid.NewGuid();
        }
    }
}