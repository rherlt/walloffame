﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using WallOfFame.Web.Const;
using WallOfFame.Web.Models.Base;

namespace WallOfFame.Web.Models
{
    public class Round : BaseModel
    {
        [Required]
        public Guid GameId { get; set; }
       
        public virtual Game Game { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? End { get; set; }

        [Required, MaxLength(255)]
        public string Playername { get; set; }

        [Required]
        public long Score { get; set; }

        [MaxLength(1024)]
        public string PlayerComment { get; set; }

        public bool IsApiCreated { get; set; }

        public class ScoreMapping : EntityTypeConfiguration<Round>
        {
            public ScoreMapping()
            {
                ToTable(TableNames.Round);
                HasKey(x => x.Id);
                Property(x => x.Start).IsOptional();
                Property(x => x.End).IsOptional();
                Property(x => x.Playername).HasMaxLength(255).IsRequired();
                Property(x => x.PlayerComment).HasMaxLength(1024).IsOptional();
                Property(x => x.Score).IsRequired();
                Property(x => x.IsApiCreated).IsRequired();

                HasRequired(x => x.Game)
                    .WithMany(x => x.Rounds)
                    .HasForeignKey(x => x.GameId)
                    .WillCascadeOnDelete(true);
            }


        }
    }
}