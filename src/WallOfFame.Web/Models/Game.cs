﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using WallOfFame.Web.Const;
using WallOfFame.Web.Models.Base;

namespace WallOfFame.Web.Models
{
    public class Game : BaseModel
    {
        [Required, MaxLength(256)]
        public string Name { get; set; }

        public DateTime? Created { get; set; }

        public virtual ICollection<Round> Rounds { get; set; }

        public class GameMapping : EntityTypeConfiguration<Game>
        {
            public GameMapping()
            {
                ToTable(TableNames.Game);
                HasKey(x => x.Id);
                Property(x => x.Created).IsRequired();
                Property(x => x.Name).HasMaxLength(256).IsRequired();

                HasMany(x => x.Rounds)
                    .WithRequired(x => x.Game)
                    .HasForeignKey(x => x.GameId)
                    .WillCascadeOnDelete(true);     
            }
        }
    }
}