namespace WallOfFame.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Game",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 256),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Round",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        GameId = c.Guid(nullable: false),
                        Start = c.DateTime(),
                        Created = c.DateTime(),
                        End = c.DateTime(),
                        Playername = c.String(nullable: false, maxLength: 255),
                        Score = c.Long(nullable: false),
                        PlayerComment = c.String(maxLength: 1024),
                        IsApiCreated = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Game", t => t.GameId, cascadeDelete: true)
                .Index(t => t.GameId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Round", "GameId", "dbo.Game");
            DropIndex("dbo.Round", new[] { "GameId" });
            DropTable("dbo.Round");
            DropTable("dbo.Game");
        }
    }
}
