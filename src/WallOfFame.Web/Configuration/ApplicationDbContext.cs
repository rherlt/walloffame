﻿using System;
using System.Data.Entity;
using System.Reflection;
using WallOfFame.Web.Models;

namespace WallOfFame.Web.Configuration
{    
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(): base()
        {
            Configuration.LazyLoadingEnabled = true;
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //Add all model configuraions from this assembly to the model builder
            //Each model configuration is defined in each model as nested class
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<WallOfFame.Web.Models.Round> Rounds { get; set; }

        public System.Data.Entity.DbSet<WallOfFame.Web.Models.Game> Games { get; set; }
    }
}