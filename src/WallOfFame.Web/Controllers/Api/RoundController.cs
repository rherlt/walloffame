﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WallOfFame.Client.Shared.DataContracts;
using WallOfFame.Client.Shared.OperationContracts;
using WallOfFame.Web.Configuration;
using WallOfFame.Web.Extensions;
using WallOfFame.Web.Filters;

namespace WallOfFame.Web.Controllers.Api
{
    [RoutePrefix(Client.Shared.Const.RoutePrefixRoundController)]
    public class RoundController : ApiController
    {      
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Round
        [HttpGet]
        [Route]
        public IHttpActionResult Get()
        {
            List<Round> rounds;
            rounds = (from r in db.Rounds
                      select r).AsEnumerable().Select(x => x.ToDataContract()).OrderByDescending(x => x.Created).ToList();

            return Ok(new GetRoundsResponse { Rounds = rounds });
        }

        [HttpGet]
        [Route(Client.Shared.Const.RouteGetAllByMinCreated)]
        public IHttpActionResult GetByDate(string minCreatedDate = null)
        {
            DateTime minCreatedDateValue;
            if (DateTime.TryParseExact(minCreatedDate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out minCreatedDateValue))
            {
                var rounds = (from r in db.Rounds
                          where r.Created >= minCreatedDateValue
                          select r).AsEnumerable().Select(x => x.ToDataContract()).OrderByDescending(x => x.Created).ToList();

                return Ok(new GetRoundsResponse { Rounds = rounds });
            }
            else
            {
                return BadRequest();
            }            
        }

        //// GET: api/Round/5
        [HttpGet]
        [Route(Client.Shared.Const.RouteGetById)]
        public IHttpActionResult Get(Guid? id = null)
        {
            var r = db.Rounds.Find(id);
            if (r != null)
            {
                return Ok(new GetRoundResponse
                {
                    Round = r.ToDataContract()
                });
            }
            else
                return NotFound();
        }

        // POST: api/Round
        [ValidationActionFilter]
        [Route]
        public IHttpActionResult Post([FromBody]PublishRoundRequest value)
        {

            if (value != null && value.Round != null)
            {
                try
                {
                    var round = value.Round.ToModel(true, db);
                    var game = db.Games.Find(round.GameId);

                    game.Rounds.Add(round);
                    db.Entry(game).State = EntityState.Modified;
                    db.SaveChanges();
                    var location = Url.Link("Default", new { id = round.Id, controller = Client.Shared.Const.RoutePrefixRoundController, action ="/" });
                    var response = round.ToDataContract();
                    return Created(location, response);
                }
                catch (Exception ex)
                {
                    return BadRequest();
                }
                
            }
            else
                return BadRequest("Argument \"round\" cannot be null.");
           
        }

        
    }
}
