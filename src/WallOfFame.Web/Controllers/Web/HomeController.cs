﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WallOfFame.Web.Controllers.Web
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "This is the wall of fame application for the Uni(versi)ty Tour 2015";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Rico Herlt";

            return View();
        }
    }
}