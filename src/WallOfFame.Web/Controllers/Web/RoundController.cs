﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WallOfFame.Web.Configuration;
using WallOfFame.Web.Models;

namespace WallOfFame.Web.Controllers.Web
{
    public class RoundController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Round
        public async Task<ActionResult> Index()
        {
            var rounds = db.Rounds.Include(r => r.Game);
            return View(await rounds.OrderByDescending(x => x.Created).ToListAsync());
        }

        // GET: Round/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Round round = await db.Rounds.FindAsync(id);
            if (round == null)
            {   
                return HttpNotFound();
            }
            return View(round);
        }

        // GET: Round/Create
        public ActionResult Create()
        {
            ViewBag.GameId = new SelectList(db.Games, "Id", "Name");
            return View();
        }

        // POST: Round/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,GameId,Start,Created,End,Playername,Score,PlayerComment")] Round round)
        {
            if (ModelState.IsValid)
            {
                round.Id = Guid.NewGuid();
                round.Created = DateTime.UtcNow;
                db.Rounds.Add(round);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.GameId = new SelectList(db.Games, "Id", "Name", round.GameId);
            return View(round);
        }

        // GET: Round/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Round round = await db.Rounds.FindAsync(id);
            if (round == null)
            {
                return HttpNotFound();
            }
            ViewBag.GameId = new SelectList(db.Games, "Id", "Name", round.GameId);
            return View(round);
        }

        // POST: Round/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,GameId,Start,Created,End,Playername,Score,PlayerComment")] Round round)
        {
            if (ModelState.IsValid)
            {
                db.Entry(round).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.GameId = new SelectList(db.Games, "Id", "Name", round.GameId);
            return View(round);
        }

        // GET: Round/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Round round = await db.Rounds.FindAsync(id);
            if (round == null)
            {
                return HttpNotFound();
            }
            return View(round);
        }

        // POST: Round/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            Round round = await db.Rounds.FindAsync(id);
            db.Rounds.Remove(round);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
