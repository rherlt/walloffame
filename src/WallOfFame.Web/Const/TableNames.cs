﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallOfFame.Web.Const
{
    public static class TableNames
    {
        public const string Game = "Game";
        public const string Round = "Round";
    }
}