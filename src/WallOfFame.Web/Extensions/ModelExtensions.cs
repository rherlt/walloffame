﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using dc = WallOfFame.Client.Shared.DataContracts;
using WallOfFame.Web.Models;
using WallOfFame.Web.Configuration;

namespace WallOfFame.Web.Extensions
{
    public static class ModelExtensions
    {
        public static dc.Round ToDataContract(this Round r)
        {
            return new dc.Round
            {
                Created = (r.Created.HasValue) ? r.Created.Value.ToUniversalTime().AsNullable() : null,
                End = (r.End.HasValue) ? r.End.Value.ToUniversalTime().AsNullable() : null,
                GameId = r.GameId,
                Id = r.Id,
                PlayerComment = r.PlayerComment,
                Playername = r.Playername,
                Score = r.Score,
                Start = (r.Start.HasValue) ? r.Start.Value.ToUniversalTime().AsNullable() : null,
                GameName = r.Game.Name
            };
        }

        public static Round ToModel(this dc.PublishRound r, bool isApiCreated, ApplicationDbContext db)
        {            
            return new Round
            {
                Created = DateTime.UtcNow,
                End = r.End,
                GameId = r.GameId,
                PlayerComment = r.PlayerComment,
                Playername = r.Playername,
                Score = r.Score,
                Start = r.Start,
                IsApiCreated = isApiCreated                
            };
        }

        public static T? AsNullable<T>(this T val) where T : struct
        {
            return val;
        }
    }
}