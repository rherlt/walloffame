﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WallOfFame.Client.Shared.DataContracts;

namespace WallOfFame.Client.Shared.OperationContracts
{
    public class GetRoundResponse
    {
        [JsonProperty("round")]
        public Round Round { get; set; }
    }
}
