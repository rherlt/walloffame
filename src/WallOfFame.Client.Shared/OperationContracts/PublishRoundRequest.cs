﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WallOfFame.Client.Shared.DataContracts;

namespace WallOfFame.Client.Shared.OperationContracts
{
    public class PublishRoundRequest
    {
        [JsonProperty("round")]
        public PublishRound Round { get; set; }
    }
}
