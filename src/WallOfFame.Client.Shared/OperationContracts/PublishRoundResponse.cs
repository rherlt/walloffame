﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace WallOfFame.Client.Shared.OperationContracts
{
    public class PublishRoundResponse
    {
        public HttpStatusCode? HttpStatusCode { get; set; }

        public string Message { get; set; }

        public bool IsSuccessStatusCode { get; set; }
    }
}
