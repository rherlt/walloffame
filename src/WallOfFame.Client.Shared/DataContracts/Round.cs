﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WallOfFame.Client.Shared.DataContracts
{
    public class Round : PublishRound
    {
        [JsonProperty("created")]
        public DateTime? Created { get; set; }

        [JsonProperty("gameName")]
        public string GameName { get; set; }
        
    }
}