﻿using Newtonsoft.Json;
using System;
using WallOfFame.Client.Shared.DataContracts.Base;

namespace WallOfFame.Client.Shared.DataContracts
{
    public class PublishRound : BaseModel
    {
        [JsonProperty("gameId")]
        public Guid GameId { get; set; }

        [JsonProperty("start")]
        public DateTime? Start { get; set; }
              
        [JsonProperty("end")]
        public DateTime? End { get; set; }

        [JsonProperty("playername")]
        public string Playername { get; set; }

        [JsonProperty("score")]
        public long Score { get; set; }

        [JsonProperty("playerComment")]
        public string PlayerComment { get; set; }
    }
}