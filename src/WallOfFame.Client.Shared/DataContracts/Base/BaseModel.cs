﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WallOfFame.Client.Shared.DataContracts.Base
{
    public class BaseModel
    {
        [JsonProperty("id")]
        public Guid? Id { get; set; }
    }
}
