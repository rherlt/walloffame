﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WallOfFame.Client.Shared.DataContracts.Base;

namespace WallOfFame.Client.Shared.DataContracts
{
    public class Game : BaseModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created")]
        public DateTime? Created { get; set; }

        [JsonProperty("rounds")]
        public List<Round> Rounds { get; set; }
        
    }
}