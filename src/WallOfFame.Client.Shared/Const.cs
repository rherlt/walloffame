﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WallOfFame.Client.Shared
{
    public static class Const
    {
        public const string BaseUrl = "http://walloffame.azurewebsites.net/Api/";
        //public const string BaseUrl = "http://localhost:50642/Api/";
        public const string PostRoundUrl = "Round";
        public const string GetRoundsUrl = "Round";
        public const string GetRoundsByMinCreatedUrl = "Round/Created/{0}";
        public const string GetRoundUrl = "Round/{0}";


        public const string RoutePrefixRoundController = "Api/Round";
        public const string RouteGetById = "{id:Guid?}";
        public const string RouteGetAllByMinCreated = "Created/{minCreatedDate}";
    }
}
