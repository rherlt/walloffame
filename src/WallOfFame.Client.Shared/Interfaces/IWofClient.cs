﻿using System;
using System.Collections.Generic;
using System.Text;
using WallOfFame.Client.Shared.OperationContracts;

namespace WallOfFame.Client.Shared.Interfaces
{
    public interface IWofClient
    {

        Guid GameId { get; }
        void StartRound();
        PublishRoundResponse EndRound(string playerName, string playerComment, long score);
        GetRoundsResponse GetRounds(DateTime? minCreatedDate);
        GetRoundsResponse GetRounds();
        GetRoundResponse GetRound(Guid id);

    }
}
