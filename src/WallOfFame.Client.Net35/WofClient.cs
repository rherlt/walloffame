﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using WallOfFame.Client.Shared;
using WallOfFame.Client.Shared.DataContracts;
using WallOfFame.Client.Shared.Interfaces;
using WallOfFame.Client.Shared.OperationContracts;

namespace WallOfFame.Client.Net35
{
    public class WofClient : IWofClient
    {
        public WofClient(string gameId)
        {
            try
            {
                GameId = new Guid(gameId);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("The argument gameId is not a valid Guid.", ex);
            }
           
        }

        public Guid GameId { get; private set; }
        private PublishRound _round;

        public void StartRound()
        {
            _round = new PublishRound
            {
                Start = DateTime.UtcNow,
                GameId = GameId,
            };
        }


        public PublishRoundResponse EndRound(string playerName, string playerComment, long score)
        {

            if (_round == null)
                StartRound();

            _round.End = DateTime.UtcNow;
            _round.PlayerComment = playerComment;
            _round.Playername = playerName;
            _round.Score = score;

            try
            {
                var body = new PublishRoundRequest { Round = _round };
                var response = PostData(Const.PostRoundUrl, body);
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                var result = new PublishRoundResponse
                {
                    HttpStatusCode = response.StatusCode,
                    IsSuccessStatusCode = (response.StatusCode == HttpStatusCode.Created),
                    Message = response.StatusDescription
                };

                return result;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                var result = new PublishRoundResponse
                {
                    HttpStatusCode = null,
                    IsSuccessStatusCode = false,
                    Message = ex.Message
                };

                return result;
            }
        }

        private static HttpWebResponse PostData(string url, object body)
        {
            return HttpRequest(url, body, "POST");
        }

        private static HttpWebResponse GetData(string url)
        {
           return HttpRequest(url, null, "GET");
        }

        private static HttpWebResponse HttpRequest(string url, object body, string method)
        {
            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", Const.BaseUrl, url));
            
            request.Method = method;
            request.Accept = "application/json";
            if (method == "POST")
            {
                string postData = JsonConvert.SerializeObject(body, Formatting.Indented);
                var data = Encoding.UTF8.GetBytes(postData);

                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
            }
            var response = (HttpWebResponse)request.GetResponse();
            return response;
        }

        public GetRoundsResponse GetRounds()
        {
            return GetRounds(null);
        }

        public GetRoundsResponse GetRounds(DateTime? minCreatedDate)
        {

            string url = (minCreatedDate.HasValue) ? string.Format(Const.GetRoundsByMinCreatedUrl, string.Format("{0:yyyy}{0:MM}{0:dd}{0:HH}{0:mm}{0:ss}", minCreatedDate.Value)) : Const.GetRoundsUrl;
            var response = GetData(url);
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            try
            {
                var rounds = JsonConvert.DeserializeObject<GetRoundsResponse>(responseString);
                return rounds;
            }
            catch (Exception)
            {

                return new GetRoundsResponse() { Rounds = new List<Round>() };
            }
        }

        public GetRoundResponse GetRound(Guid id)
        {
            string url = string.Format(Const.GetRoundUrl, id.ToString("D"));
            var response = GetData(url);
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            try
            {
                var round = JsonConvert.DeserializeObject<GetRoundResponse>(responseString);
                return round;
            }
            catch (Exception)
            {
                return new GetRoundResponse() { Round = new Round() };
            }
        }
    }
}
